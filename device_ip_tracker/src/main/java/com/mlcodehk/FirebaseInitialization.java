package com.mlcodehk;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import java.io.*;
import java.net.URI;

public class FirebaseInitialization {

    public static void init(InputStream is) throws IOException {

        FirebaseOptions options =
                FirebaseOptions.builder()
                        .setCredentials(GoogleCredentials.fromStream(is))
                        .build();

        FirebaseApp.initializeApp(options);
    }

}
