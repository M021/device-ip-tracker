package com.mlcodehk;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Date;

public class Main {

    private static final String PROJECT_ID = "device-ip-tracker";
    private static final InputStream credentialJsonIs = Main.class.getClassLoader().getResourceAsStream("credential/firebase_credential.json");
    private static final String checkIpEndpoint = "https://checkip.amazonaws.com/";

    public static void main(String[] args) {

        if (args == null || args.length < 1) {
            System.out.println("[Usage] java -jar xx.jar <device_name> <time_interval_in_second>");
            System.out.println("time_interval_in_second: empty indicates only run 1 time. Otherwise, it will repeat based on the time interval.");
            return;
        }

        String deviceName = null;
        Integer timeInterval = null;
        boolean isInitialized = false;
        FirestoreService service = null;
        int cnt = 1;

        do {
            System.out.println("=================================================");
            System.out.println("Trial : " + cnt++);
            try {
                deviceName = args[0];
                timeInterval = args.length == 2 ? Integer.valueOf(args[1]) : null;
                String ip = checkIP();

                if(!isInitialized) {
                    FirebaseInitialization.init(credentialJsonIs);
                    service = new FirestoreService(PROJECT_ID);
                    isInitialized = true;
                }

                service.addDocument(deviceName, ip, new Date());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (timeInterval == null) {
                break;
            }

            try {
                Thread.sleep(timeInterval * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } while (true);

    }

    public static String checkIP() throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .connectTimeout(Duration.ofSeconds(10))
                .build();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(checkIpEndpoint))
                .build();
        HttpResponse<String> response = httpClient.send(request,
                HttpResponse.BodyHandlers.ofString());

        System.out.println("Status code: " + response.statusCode());
        System.out.println("Response body: " + response.body());

        return response.body();
    }
}
