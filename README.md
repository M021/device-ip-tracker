# IP recorder

This project is to record the IP of a device in Firestore database so that the deployer can keep track on what the device's ip is remotely.

- [Description](#description)
- [Resources](#resources)
- [Installation](#installation)
  - [Firestore Setup](#firestore-setup)
  - [Source code update](#source-code-update)
  - [Java version](#java-version)
  - [Build a jar](#build-a-jar)
- [Usage](#usage)
- [Result](#result)



## Description

There are some ways to get the ip of a device remotely.
1. Use DDNS. However, some DDNS service cost you money. Although some DDNS service are free, I am not sure if they are trustworthy.
2. Set up a server with a API to record the IP in database and display it on a web page. The server and database may cost money. Also, it needs more effort.
3. Use a free database Firestore which is owned by Google. It is relatively trustworthy. Using the library provided by firebase, data can be directly inserted to the database and  the data can be accessed remotely via firebase console. Also, the jar program can be deployed to different devices and  their IP can be recored and monitored via firebase console.

This project is only used to record IP. However, by using the same concept, you can also record other information by modifying the code.

## Resources
Firestore provides a free database. [https://firebase.google.com/docs/firestore/](https://firebase.google.com/docs/firestore/)

## Installation
### Firestore Setup
You may take reference from below video to realise how to set up a project and firestore. You may see the content from 00:00 - 1:05. The video is not created by me. The credit should be given to the creator "The Net Ninja".
[https://www.youtube.com/watch?v=2yNyiW_41H8](https://www.youtube.com/watch?v=2yNyiW_41H8)

After setting up the firestore, you do not need to create any collection in the firestore. The program will do it for you.

### Source code update
There are some configurations in Main.java

    //update to your project id
    private static final String PROJECT_ID = "device-ip-tracker";

    //put your credential under the directory credential before building a jar.
    private static final InputStream credentialJsonIs = Main.class.getClassLoader().getResourceAsStream("credential/firebase_credential.json");

    // you may use your own ip check service
    private static final String checkIpEndpoint = "https://checkip.amazonaws.com/";

The credential json file is similar to below. Please use your own credential for your firebase authentication

{
  "type": "service_account",
  "project_id": "xxxxxx",
  "private_key_id": "xxxxxxx",
  "private_key": "-----BEGIN PRIVATE KEY----- xxxxxxxxxxxxxxxx",
  "client_email": "xxxxxxxxx",
  "client_id": "xxxxxxxxxxxx",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "xxxxxxxxxxxxxxxxxx"
}

### Java version
Java 17

### Build a jar
use maven to build a jar. pom.xml has been configured to include all the dependencies (jar-with-dependencies).

## Usage
     java -jar <jar-filename>.jar <device_name> <time_interval_in_second>

time_interval_in_second: empty indicates only run 1 time. Otherwise, it will repeat based on the time interval.

## Result
- ![](firestore_console.png)